package net.schoeninger.petstoreserver.controllers;

import net.schoeninger.petstore.api.PetApi;
import net.schoeninger.petstore.model.Pet;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PetController implements PetApi {

    @Override
    public ResponseEntity<Void> addPet(Pet body) {
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @Override
    public ResponseEntity<Pet> getPetById(Long petId) {
        Pet mockPet = new Pet();
        mockPet.setId(1L);
        mockPet.setName("Teddy");
        mockPet.setStatus(Pet.StatusEnum.AVAILABLE);
        return ResponseEntity.ok(mockPet);
    }

}
