package net.schoeninger.petstoreserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PetStoreServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(PetStoreServerApplication.class, args);
    }

}
