package net.schoeninger.petstoreclient;

import lombok.extern.slf4j.Slf4j;
import net.schoeninger.petstore.api.PetApi;
import net.schoeninger.petstore.model.Pet;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@EnableScheduling
public class TestWebClient {

    private final PetApi petApi;

    public TestWebClient(PetApi petApi) {
        this.petApi = petApi;
    }

    @Scheduled(fixedDelay = 5000)
    public void scheduledTask() {
        Pet pet = petApi.getPetById(1L).block();
        log.info("Received pet: " + pet);
    }

}
