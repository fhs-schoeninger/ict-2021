package net.schoeninger.petstoreclient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PetStoreClientApplication {

    public static void main(String[] args) {
        SpringApplication.run(PetStoreClientApplication.class, args);
    }

}
