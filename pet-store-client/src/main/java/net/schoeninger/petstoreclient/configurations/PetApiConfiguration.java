package net.schoeninger.petstoreclient.configurations;

import net.schoeninger.petstore.api.PetApi;
import net.schoeninger.petstore.invoker.ApiClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class PetApiConfiguration {

    @Bean
    public PetApi petApi(@Value("${pet-store-server.url}") String basePath) {
        // Configure the base url, authentication, common http headers, etc. here:
        ApiClient apiClient = new ApiClient();
        apiClient.setBasePath(basePath);
        return new PetApi(apiClient);
    }

}
