# Messaging

## Spring Profiles:

* p2p: Point-to-Point example
* pubSub: Publish-Subscribe example

## RabbitMQ

* Management Interface: http://localhost:15672
* Username / Password: guest/guest

## Docker Compose

Only start RabbitMQ:
```
docker-compose -f docker-compose-mq-only.yml up
```

Point-to-Point Example:
```
docker-compose --env-file .env.p2p up --scale consumer=3
```

Publish-Subscribe Example:
```
docker-compose --env-file .env.pubSub up --scale consumer=3
```

RPC Example:
```
docker-compose --env-file .env.rpc up --scale producer=3
```
