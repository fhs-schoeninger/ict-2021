package net.schoeninger.producer.point2point;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Profile("p2p")
@Slf4j
@Component
@EnableScheduling
public class SenderTask {

    private final RabbitTemplate rabbitTemplate;

    public SenderTask(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    @Scheduled(fixedDelay = 2000)
    public void send() {
        rabbitTemplate.convertAndSend("point2point", "Point2Point-Message");
        log.info("Sent message");
    }

}
