package net.schoeninger.producer.pubSub;

import org.springframework.amqp.core.FanoutExchange;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Profile("pubSub")
@Configuration
public class PubSubConfig {

    @Bean
    public FanoutExchange fanoutExchange() {
        return new FanoutExchange("tut.fanout");
    }

}
