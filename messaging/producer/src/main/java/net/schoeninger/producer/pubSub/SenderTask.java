package net.schoeninger.producer.pubSub;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Profile("pubSub")
@Slf4j
@Component
@EnableScheduling
public class SenderTask {

    private final RabbitTemplate rabbitTemplate;
    private final FanoutExchange fanoutExchange;

    public SenderTask(RabbitTemplate rabbitTemplate, FanoutExchange fanoutExchange) {
        this.rabbitTemplate = rabbitTemplate;
        this.fanoutExchange = fanoutExchange;
    }

    @Scheduled(fixedDelay = 2000)
    public void send() {
        rabbitTemplate.convertAndSend(fanoutExchange.getName(), "","PubSub-Message");
        log.info("Sent message");
    }

}
