package net.schoeninger.producer.rpc;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Profile("rpc")
@Configuration
public class RpcConfig {

    @Bean
    public Queue queue() {
        return new Queue("rpc.requests");
    }

    @Bean
    public DirectExchange directExchange() {
        return new DirectExchange("rpc");
    }

    @Bean
    public Binding binding(DirectExchange directExchange, Queue queue) {
        return BindingBuilder
                .bind(queue)
                .to(directExchange)
                .with("rpc");
    }

}
