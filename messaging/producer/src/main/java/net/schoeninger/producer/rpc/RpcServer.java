package net.schoeninger.producer.rpc;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Profile("rpc")
@Slf4j
@Component
public class RpcServer {

    @RabbitListener(queues = "rpc.requests")
    public int square(int n) {
        log.info("Received request with n=" + n);
        int result = n*n;
        log.info("Returning " + result);
        return result;
    }

}
