package net.schoeninger.producer.point2point;

import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Profile("p2p")
@Configuration
public class Point2PointConfig {

    @Bean
    public Queue queue() {
        return new Queue("point2point");
    }

}
