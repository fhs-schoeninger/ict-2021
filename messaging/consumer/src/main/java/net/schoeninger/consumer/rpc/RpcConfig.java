package net.schoeninger.consumer.rpc;

import org.springframework.amqp.core.DirectExchange;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Profile("rpc")
@Configuration
public class RpcConfig {

    @Bean
    public DirectExchange directExchange() {
        return new DirectExchange("rpc");
    }

}
