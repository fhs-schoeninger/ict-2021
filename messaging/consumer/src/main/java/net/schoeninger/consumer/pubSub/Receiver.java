package net.schoeninger.consumer.pubSub;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Profile("pubSub")
@Slf4j
@Component
@RabbitListener(queues = "#{autoDeleteQueue.name}")
public class Receiver {

    @RabbitHandler
    public void receiveMessage(String message) {
        log.info("Received message: " + message);
    }

}
