package net.schoeninger.consumer.rpc;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Profile("rpc")
@Slf4j
@Component
@EnableScheduling
public class RpcClient {

    private final RabbitTemplate rabbitTemplate;
    private final DirectExchange directExchange;
    private int n = 0;

    public RpcClient(RabbitTemplate rabbitTemplate, DirectExchange directExchange) {
        this.rabbitTemplate = rabbitTemplate;
        this.directExchange = directExchange;
    }

    @Scheduled(fixedDelay = 2000)
    public void send() {
        log.info("Requesting square(" + n + ")");
        Integer result = (Integer) rabbitTemplate.convertSendAndReceive(directExchange.getName(), "rpc", n);
        log.info("Received: " + result);
        n++;
    }

}
