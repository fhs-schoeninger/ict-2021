package net.schoeninger.consumer.point2point;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Profile("p2p")
@Slf4j
@Component
@RabbitListener(queues = "point2point")
public class Receiver {

    @RabbitHandler
    public void receiveMessage(String message) {
        log.info("Received message: " + message);
    }

}
