package net.schoeninger.messaginglabexample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MessagingLabExampleApplication {

    public static void main(String[] args) {
        SpringApplication.run(MessagingLabExampleApplication.class, args);
    }

}
