package net.schoeninger.messaginglabexample;

import lombok.extern.slf4j.Slf4j;
import net.schoeninger.messaginglabexample.events.StreamStartedEvent;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;

@Slf4j
@Component
@EnableScheduling
public class Publisher {

    private final RabbitTemplate rabbitTemplate;
    private final FanoutExchange fanoutExchange;

    public Publisher(RabbitTemplate rabbitTemplate, FanoutExchange fanoutExchange) {
        this.rabbitTemplate = rabbitTemplate;
        this.fanoutExchange = fanoutExchange;
    }

    @Scheduled(fixedDelay = 5000)
    public void publish() {
        StreamStartedEvent event = StreamStartedEvent.builder()
                .songId(1L)
                .user("usr01")
                .timestamp(new Date())
                .build();
        rabbitTemplate.convertAndSend(fanoutExchange.getName(), "", event);
        log.info("Published message: " + event);
    }

}
