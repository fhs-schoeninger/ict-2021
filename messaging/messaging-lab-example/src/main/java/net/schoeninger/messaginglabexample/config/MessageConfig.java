package net.schoeninger.messaginglabexample.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MessageConfig {

    /*
     * Config for both the publisher & subscriber:
     */

    @Bean
    public FanoutExchange fanoutExchange() {
        return new FanoutExchange("labor.fanout");
    }

    @Bean
    public Jackson2JsonMessageConverter jackson2JsonMessageConverter() {
        return new Jackson2JsonMessageConverter();
    }

    /*
     * Config for the publisher:
     */

    @Bean
    public RabbitTemplate rabbitTemplate(ConnectionFactory connectionFactory) {
        RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
        rabbitTemplate.setMessageConverter(jackson2JsonMessageConverter());
        return rabbitTemplate;
    }

    /*
     * Config for the consumer:
     */

    @Bean
    public Queue receiverQueue() {
        return new Queue("labor.fanout.queue");
    }

    @Bean
    public Binding binding(FanoutExchange fanoutExchange, Queue recommenderQueue) {
        return BindingBuilder.bind(recommenderQueue).to(fanoutExchange);
    }

}
