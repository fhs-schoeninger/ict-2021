package net.schoeninger.messaginglabexample.events;

import lombok.*;

import java.util.Date;

@NoArgsConstructor @AllArgsConstructor @Getter @Builder @ToString
public class StreamStartedEvent {

    private long songId;
    private String user;
    private Date timestamp;

}
