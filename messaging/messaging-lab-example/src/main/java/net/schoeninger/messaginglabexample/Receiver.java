package net.schoeninger.messaginglabexample;

import lombok.extern.slf4j.Slf4j;
import net.schoeninger.messaginglabexample.events.StreamStartedEvent;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@RabbitListener(queues = "#{receiverQueue.name}")
public class Receiver {

    @RabbitHandler
    public void receiveMessage(final StreamStartedEvent streamStartedEvent) {
        log.info("Received message: " + streamStartedEvent);
    }

}
