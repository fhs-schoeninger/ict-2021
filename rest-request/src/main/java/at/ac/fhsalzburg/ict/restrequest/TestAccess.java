package at.ac.fhsalzburg.ict.restrequest;

import at.ac.fhsalzburg.ict.restrequest.api.DummyControllerApi;
import at.ac.fhsalzburg.ict.restrequest.model.Dummy;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.scheduling.annotation.Schedules;
import org.springframework.stereotype.Component;

import java.util.Random;

@Slf4j
@AllArgsConstructor
@Component
@EnableScheduling
public class TestAccess {
    private DummyControllerApi dummyControllerApi;

    @Scheduled(fixedDelay = 5000)
    public void putRequestToConsole() {
        Dummy dummy = dummyControllerApi.getDummy((new Random()).nextInt()).block();
        log.info("Das ist mein Testdummy:\n" + dummy );
    }
}
