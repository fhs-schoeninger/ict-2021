package at.ac.fhsalzburg.ict.restrequest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestRequestApplication {

    public static void main(String[] args) {
        SpringApplication.run(RestRequestApplication.class, args);
    }

}
