package at.ac.fhsalzburg.ict.restrequest.configuration;

import at.ac.fhsalzburg.ict.restrequest.api.DummyControllerApi;
import at.ac.fhsalzburg.ict.restrequest.invoker.ApiClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DummyAppConfiguration {
    @Bean
    public DummyControllerApi getApi(@Value("${dummy-api-server.url}") String basePath) {
        ApiClient apiClient = new ApiClient();
        apiClient.setBasePath(basePath);

        return new DummyControllerApi(apiClient);
    }
}
