package net.schoeninger.taskapp.services;

import net.schoeninger.taskapp.domain.Task;

import java.util.List;

public interface TaskService {

    List<Task> getAllTasksForProject(long projectId);
    List<Task> getTasksForProject(long projectId, boolean filterDone);
    Task addTaskToProject(long projectId, Task newTask);
    Task updateTaskForProjectWithId(long projectId, long taskId, Task newValues);
    void deleteTaskForProjectById(long projectId, long taskId);

}
