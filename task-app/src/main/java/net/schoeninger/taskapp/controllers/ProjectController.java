package net.schoeninger.taskapp.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import net.schoeninger.taskapp.domain.Project;
import net.schoeninger.taskapp.services.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Tag(name = "Projects", description = "Project CRUD API")
@RestController
@RequestMapping("/api/v1/projects")
public class ProjectController {

    private final ProjectService projectService;

    @Autowired
    public ProjectController(ProjectService projectService) {
        this.projectService = projectService;
    }

    @Operation(summary = "Get a list of all existing projects.")
    @GetMapping
    public List<Project> getAll() {
        return projectService.findAll();
    }

    @Operation(summary = "Get a Project by its id.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Project found"),
            @ApiResponse(responseCode = "404", description = "Project not found", content = @Content) // empty content
    })
    @GetMapping("/{projectId}")
    public Project getById(@PathVariable long projectId) {
        return projectService.findById(projectId);
    }

    @Operation(summary = "Create a new Project.")
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Project addNew(@RequestBody Project newProject) {
        return projectService.addNew(newProject);
    }

    @Operation(summary = "Update the Project with the given id.")
    @PutMapping("/{projectId}")
    public Project updateForId(@PathVariable long projectId, @RequestBody Project newValues) {
        return projectService.updateExisting(projectId, newValues);
    }

    @Operation(summary = "Delete the Project with the given id.")
    @DeleteMapping("/{projectId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteForId(@PathVariable long projectId) {
        projectService.deleteById(projectId);
    }

}
