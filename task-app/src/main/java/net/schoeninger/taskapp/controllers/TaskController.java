package net.schoeninger.taskapp.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import net.schoeninger.taskapp.domain.Task;
import net.schoeninger.taskapp.services.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Tag(name = "Tasks", description = "Tasks CRUD API")
@RestController
@RequestMapping("/api/v1/projects")
public class TaskController {

    private TaskService taskService;

    @Autowired
    public TaskController(TaskService taskService) {
        this.taskService = taskService;
    }

    @Operation(summary = "Get a list of Tasks for the given projectId.")
    @GetMapping("/{projectId}/tasks")
    public List<Task> getAllTasksForProject(@PathVariable long projectId) {
        return taskService.getAllTasksForProject(projectId);
    }

    @Operation(summary = "Get a list of Tasks for the given projectId filtered by the 'done' request parameter.")
    @GetMapping(value = "/{projectId}/tasks", params = "done")
    public List<Task> getFilteredTasksForProject(@PathVariable long projectId, @RequestParam boolean done) {
        return taskService.getTasksForProject(projectId, done);
    }

    @Operation(summary = "Create a new Task and and assign it to the project with the given 'projectId'.")
    @PostMapping("/{projectId}/tasks")
    @ResponseStatus(HttpStatus.CREATED)
    public Task addTaskToProject(@PathVariable long projectId, @RequestBody @Valid Task newTask) {
        return taskService.addTaskToProject(projectId, newTask);
    }

    @Operation(summary = "Update the Task with the given id.")
    @PutMapping("/{projectId}/tasks/{taskId}")
    public Task updateById(@PathVariable long projectId, @PathVariable long taskId, @RequestBody @Valid Task newValues) {
        return taskService.updateTaskForProjectWithId(projectId, taskId, newValues);
    }

    @Operation(summary = "Delete the Task with the given id.")
    @DeleteMapping("/{projectId}/tasks/{taskId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteById(@PathVariable long projectId, @PathVariable long taskId) {
        taskService.deleteTaskForProjectById(projectId, taskId);
    }

}
