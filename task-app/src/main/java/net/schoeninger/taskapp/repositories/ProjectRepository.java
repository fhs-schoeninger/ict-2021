package net.schoeninger.taskapp.repositories;

import net.schoeninger.taskapp.domain.Project;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProjectRepository extends JpaRepository<Project, Long> {
    // TODO: Checkout Pageable
}
