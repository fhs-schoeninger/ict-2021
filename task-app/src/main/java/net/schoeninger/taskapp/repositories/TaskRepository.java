package net.schoeninger.taskapp.repositories;

import net.schoeninger.taskapp.domain.Project;
import net.schoeninger.taskapp.domain.Task;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface TaskRepository extends JpaRepository<Task, Long> {
    List<Task> findAllByProject(Project project);

    @Query("select t from Task t where t.project = ?1 and t.doneAt is null")
    List<Task> findAllOpenTaskyByProject(Project project);
//    List<Task> findAllByProjectAndDoneAtIsNull(Project project);

    @Query("select t from Task t where t.project = ?1 and t.doneAt is not null")
    List<Task> findAllDoneTasksByProject(Project project);
//    List<Task> findAllByProjectAndDoneAtIsNotNull(Project project);

    Optional<Task> findByProjectAndId(Project project, long taskId);

    // TODO: Checkout Pageable

}
