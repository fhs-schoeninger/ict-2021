# Template for Lab 01

Markdown Cheatsheet: https://about.gitlab.com/handbook/markdown-guide

## Requirements Engineering

### Howto

1. Create the labels **User Story**, **Command**, **Query**.
2. Perform all the tasks up to section **Define APIs and Collaboration**.
3. Create a label for each identified service.
4. Perform the remaining tasks.

### Functional Requirements

https://www.atlassian.com/agile/project-management/user-stories

- [x] [User Stories](https://gitlab.com/fhs-schoeninger/ict-2021/-/issues?label_name%5B%5D=User+Story)

### High-level Domain Model

- [ ] UML Domain Model Diagram
- [ ] Textual description of the diagram

Partial Domain Model ([plantuml](https://docs.gitlab.com/ee/administration/integration/plantuml.html) example)

```plantuml
Playlist o-- Song
```
<details>
  <summary markdown="span">Show Markdown Code</summary>
  <pre>
    ```plantuml
    Playlist o-- Song
    ```</pre></details>
    
### System Operations

- [ ] [System Commands](https://gitlab.com/fhs-schoeninger/ict-2021/-/issues?label_name%5B%5D=Command)
- [ ] [System Queries](https://gitlab.com/fhs-schoeninger/ict-2021/-/issues?label_name%5B%5D=Query)

### Identify Services

- [ ] Identify Subdomains / Services
  - [PlaylistService](https://gitlab.com/fhs-schoeninger/ict-2021/-/issues?label_name%5B%5D=PlaylistService): Service for managing playlists (add, update, delete, add songs,...).

### Define APIs and Collaboration

- [ ] Assign system operations to services
- [ ] Define collaboration
- [ ] UML Component Diagram
- [ ] Textual description of the diagram
