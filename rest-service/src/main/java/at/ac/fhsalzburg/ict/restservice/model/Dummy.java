package at.ac.fhsalzburg.ict.restservice.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor @Getter @Setter @Builder
public class Dummy {
    private String key;
    private Integer value;
}
