package at.ac.fhsalzburg.ict.restservice.controller;

import at.ac.fhsalzburg.ict.restservice.model.Dummy;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/dummy")
@AllArgsConstructor
public class DummyController {

    @GetMapping("/{value}")
    public Dummy getDummy(@PathVariable Integer value) {
        return new Dummy("key", value);
    }
}
